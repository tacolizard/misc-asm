section .data
	hello db 'Hello x64 world!',0xa

section .text
	global _start

_start:
	mov rdi,hello
	call print

	jmp exit

print: ;print string to stdout
	push rdi;get input
	call strlen;calc length of rdi
	mov rdi,1;file descriptor (stdout)
	pop rsi
	mov rdx,rax
	mov rax,1;select syscall (sys_write)
	syscall
	ret

print_err: ;print error to stderr
	push rdi
	call strlen
	mov rdi,2
	pop rsi
	mov rdx,rax
	mov rax,1
	syscall
	call error_exit
	ret

strlen:;get the length of a string
	xor rax,rax;zero rax
.strlen_loop:
	cmp BYTE [rdi + rax],0xa;compare byte to newline
	je .strlen_break;break on newline
	inc rax
	jmp .strlen_loop;repeat if no newline found
.strlen_break:
	inc rax;increment length to count the newline
	ret

error_exit:
	mov rax,60
	mov rdi,1
	syscall

exit:
	mov rax,60
	mov rdi,0
	syscall
