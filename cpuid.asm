section .data
	nonumber db 'No number.'
	nlen equ $ - nonumber
	yesnumber db 'Yes number.'
	ylen equ $ - yesnumber

section .bss
	PRN resb 2

section .text
	global _start

_start:
mov     Ah, 00h   ; interrupt to get system timer in CX:DX 
int     1Ah
mov     [PRN], dx
call    CalcNew   ; -> AX is a random number
xor     dx, dx
mov     cx, 10    
div     cx        ; here dx contains the remainder - from 0 to 9
add     dl, '0'   ; to ascii from '0' to '9'
mov     ah, 02h   ; call interrupt to display a value in DL
int     21h    
call    CalcNew   ; -> AX is another random number
ret

; ----------------
; inputs: none  (modifies PRN seed variable)
; clobbers: DX.  returns: AX = next random number
CalcNew:
    mov     ax, 25173          ; LCG Multiplier
    mul     [PRN],ax     ; DX:AX = LCG multiplier * seed
    add     ax, 13849          ; Add LCG increment value
    ; Modulo 65536, AX = (multiplier*seed+increment) mod 65536
    mov     [PRN], ax          ; Update seed = return value
    ret
exit:
	mov ebx,0
	mov eax,1
	int 0x80
