rand:
    mov rax,[next]       ; pass next to rax for multiplication
    rdtsc
    ;mov rbx,1103515245   ; the multiplier
    mov rbx,rax
    mul rbx               ; rax = rax * rbx
    add rax,12345        ; the increment 
    mov [next],rax ; update next value
    mov rbx,13337        ; the modulus 
    xor rdx,rdx          ; avoid Floating point exception
    div rbx               ; rdx now holds the random number
    mov rax,rdx
    ret                   ; bye
srand:
    call get_time
    mov [next],rax ; initialize next
    ret                   ; bye 
    
get_time:;gets epoch time and stores it in rax
    mov rax,201
    xor rdi,rdi
    syscall
    ret

check_rdrand: ;check if cpu supports the rdrand instruction
    mov ecx,0x01
    cpuid
    lea r15,[gbuf]
    mov [ecxFlags],ecx
    mov [gbuf],ecx
    shr dword [gbuf],30;(num >> shift) & 1
    and dword [gbuf],1
    cmp dword [gbuf],1
    jne .continue
    mov byte [supportsRdRand],1
    mov rax,rdrandStr
    call println
.continue:
    ret
