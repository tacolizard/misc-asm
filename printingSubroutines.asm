section .data
    BYTE_BUFFER times 10 db 0

    debugStr db 'Debug message.',0x0
    newlineStr db 0xA,0x0

print_err:;print error
    push rax
    call strlen
    push rdi
    mov rdi,2;set fd to stderr
    pop rdx
    pop rsi
    mov rax,1
    syscall
    call error_exit

println:
    call print
    call newline
    ret
    
print:;takes input in rax
    push rax
    call strlen
    push rdi
    mov rdi,1
    pop rdx
    pop rsi
    mov rax,1
    syscall
    ret
    
print_int:
    call int_to_char
    mov rax,r9
    call print
    ret
    
println_int:
    call print_int
    call newline
    ret
    
newline:
    mov rax,newlineStr
    call print
    ret
    
;This is the function which will convert our integers back to characters
;Argument - Integer Value in rax
;Returns pointer to equivalent string (in r9)
int_to_char:
    mov rbx,10
    ;We have declared a memory which we will use as buffer to store our result
    mov r9, BYTE_BUFFER+10 ;We are are storing the number in backward order like LSB in 10 index and decrementing index as we move to MSB
    mov [r9],byte 0x0 ;Store NULL terminating byte in last slot
    dec r9 ;Decrement memory index
    mov r11,1;r11 will store the size of our string stored in buffer we will use it while printing as argument to sys_write
.loop_block:
    mov rdx,0
    div rbx    ;Get the LSB by dividing number by 10 , LSB will be remainder (stored in 'dl') lik
    cmp rax,0 ;If rax (quotient) becomes 0 our procedure reached to the MSB of the number we sho
    je .return_block
    add dl,48 ;Convert each digit to its ASCII value
    mov [r9],dl ;Store the ASCII value in memory by using r9 as index
    dec r9 ;Dont forget to decrement r9 remember we are using memory backwards
    inc r11 ;Increment size as soon as you add a digit in memory
    jmp .loop_block ;Loop until it breaks on its own
.return_block:
    add dl,48 ;Don't forget to repeat the routine for out last MSB as loop ended early
    mov [r9],dl
    ;dec r9
    inc r11
    ret
    
strlen:;get length of strings based on null bytes
    xor rdi,rdi
.strlen_loop:
    cmp byte [rax + rdi],0x0
    je .strlen_break
    inc rdi
    jmp .strlen_loop
.strlen_break:
    inc rdi
    ret
