%define ops 999

section .data
    BYTE_BUFFER times 10 db 0
    int64 dq 0

    debugStr db 'Debug message.',0x0
    rdrandStr db 'RdRand support detected.',0x0
    newlineStr db 0xA,0x0
    errorStr db 'Error:',0x0
    timeStr db 'Time: ',0x0
    cyclesStr db 'Cycles: ',0x0
    finishedStr db 'Benchmark finished.',0x0
    runningStr db 'Running',0x0
    periodStr db '.',0x0
    invalidOpStr db 'Invalid argument. Argument must be an integer.',0x0
    notEnoughArgsStr db 'You must supply an integer argument.',0xA,0x0
    opsPerformedStr db 'Total floating operations performed:',0x0
    flopsStr db '(Rough) FLOPS (Floating-point Operations Per Second):',0x0
    opsPerCycleStr db '(Rough) Cycles per operation:',0x0
    
section .bss
    startTime resq 1
    endTime resq 1
    elapsedTime resq 1
    
    startCycles resq 1
    endCycles resq 1
    elapsedCycles resq 1
    
    next resq 1
    supportsRdRand resb 1
    ecxFlags resd 1
    gbuf resq 1
    randNum resq 1
    loops resq 1
    loopsInitial resq 1
    opsPerformed resq 1
    
section .text
    global _start
    
_start:
    pop rdx;pop argc from the stack
    cmp rdx,2
    jne not_enough_args
    add rsp,8;skip argv[0], as it's just the program name
    xor rsi,rsi
    pop rsi;pop argv[1]
    call char_to_int
    mov qword [loopsInitial],rax
    mov qword [loops],rax

    call check_rdrand
    call get_time ;start epoch timer
    mov [startTime],rax
    rdtsc
    mov [startCycles],rax;start cycle timer
    call srand;generate random seed for the arithmetic
    call rand
    mov qword [randNum],rax
    call newline
    mov rax,runningStr
    call print
.bench:
    rdtsc
    mul qword [randNum];result stored in rax
    mov qword [int64],rax
    
    fild qword [int64]
    times ops fsqrt
    mov rax,periodStr
    call print
    dec qword [loops]
    cmp qword [loops],0
    jne .bench
    rdtsc
    mov [endCycles],rax;end cycle timer
    call get_time
    mov [endTime],rax ;stop epoch timer
    call newline
    mov rax,finishedStr
    call println
    mov rax,[endTime];calc elapsed time
    sub rax,[startTime]
    mov [elapsedTime],rax
    mov rax,[endCycles];calc elapsed cycles
    sub rax,[startCycles]
    mov [elapsedCycles],rax
    
    mov rax,timeStr;print time
    call print
    mov rax,qword [elapsedTime]
    call println_int
    mov rax,opsPerformedStr
    call println
    
    push qword [loopsInitial];print total flops
    pop rax
    imul rax,ops
    mov qword [opsPerformed],rax
    call println_int
    
    mov rax,opsPerCycleStr;print ops/cycle
    call println
    mov edx,0
    mov ecx,dword [opsPerformed]
    mov eax,dword [elapsedCycles]
    div ecx
    mov dword [gbuf],eax
    mov rax,qword [gbuf]
    call println_int
    
    mov rax,flopsStr;print flops
    call println
    mov ecx,dword [opsPerformed]
    mov eax,dword [elapsedTime]
    div ecx
    mov dword [gbuf],eax
    mov rax,qword [gbuf]
    call println_int
    jmp exit
    
not_enough_args:
    mov rax,notEnoughArgsStr
    call print_err
    
check_rdrand: ;check if cpu supports the rdrand instruction
    mov ecx,0x01
    cpuid
    mov [ecxFlags],ecx
    mov [gbuf],ecx
    shr dword [gbuf],30;(num >> shift) & 1
    and dword [gbuf],1
    cmp dword [gbuf],1
    jne .continue
    mov byte [supportsRdRand],1
    mov rax,rdrandStr
    call println
.continue:
    ret
    
rand:;outputs rand to rax
    cmp byte [supportsRdRand],1
    jne .no_rdrand
    rdrand rbx
    jmp .continue
.no_rdrand:
    call get_time
    mov rbx,rax
.continue:
    mov rax,[next]
    mov rbx,rax
    mul rbx               ; rax = rax * rbx
    add rax,12345        ; the increment 
    mov [next],rax ; update next value
    mov rbx,13337      ; the modulus 
    xor rdx,rdx          ; avoid Floating point exception
    div rbx               ; rdx now holds the random number
    mov rax,rdx
    ret                   ; bye
    
srand:
    rdtsc
    mov [next],rax ; initialize next
    ret                   ; bye 
    
get_time:;gets epoch time and stores it in rax
    mov rax,201
    xor rdi,rdi
    syscall
    ret
    
print_err:;print error
    push rax
    call strlen
    push rdi
    mov rdi,2;set fd to stderr
    pop rdx
    pop rsi
    mov rax,1
    syscall
    call error_exit

println:
    call print
    call newline
    ret
    
print:;takes input in rax
    push rax
    call strlen
    push rdi
    mov rdi,1
    pop rdx
    pop rsi
    mov rax,1
    syscall
    ret
    
print_int:
    call int_to_char
    mov rax,r9
    call print
    ret
    
println_int:
    call print_int
    call newline
    ret
    
newline:
    mov rax,newlineStr
    call print
    ret
    
invalid_operand:
    mov rax,invalidOpStr
    call print_err
    
;This is the function which will convert our character input to integers
;Argument - pointer to string or char ( takes rsi as argument )
;Returns equivalent integer value (in rax)
char_to_int:
    xor ax,ax ;store zero in ax
    xor cx,cx ;same
    mov bx,10 ; store 10 in bx - the input string is in base 10, so each place value increases by a factor of 10
.loop_block:
    ;REMEMBER rsi is base address to the string which we want to convert to integer equivalent

    mov cl,[rsi] ;Store value at address (rsi + 0) or (rsi + index) in cl, rsi is incremented below so dont worry about where is index.
    cmp cl,byte 0 ;If value at address (rsi + index ) is byte 0 (NULL) , means our string is terminated here
    je .return_block

    ;Each digit must be between 0 (ASCII code 48) and 9 (ASCII code 57)
    cmp cl,0x30 ;If value is lesser than 0 goto invalid operand
    jl invalid_operand
    cmp cl,0x39 ;If value is greater than 9 goto invalid operand
    jg invalid_operand

    sub cl,48 ;Convert ASCII to integer by subtracting 48 - '0' is ASCII code 48, so subtracting 48 gives us the integer value

    ;Multiply the value in 'ax' (implied by 'mul') by bx (always 10). This can be thought of as shifting the current value
    ;to the left by one place (e.g. '123' -> '1230'), which 'makes room' for the current digit to be added onto the end.
    ;The result is stored in dx:ax.
    mul bx

    ;Add the current digit, stored in cl, to the current intermediate number.
    ;The resulting sum will be mulitiplied by 10 during the next iteration of the loop, with a new digit being added onto it
    add ax,cx

    inc rsi ;Increment the rsi's index i.e (rdi + index ) we are incrementing the index

    jmp .loop_block ;Keep looping until loop breaks on its own
.return_block:
    ret
    
;This is the function which will convert our integers back to characters
;Argument - Integer Value in rax
;Returns pointer to equivalent string (in r9)
int_to_char:
    mov rbx,10
    ;We have declared a memory which we will use as buffer to store our result
    mov r9, BYTE_BUFFER+10 ;We are are storing the number in backward order like LSB in 10 index and decrementing index as we move to MSB
    mov [r9],byte 0x0 ;Store NULL terminating byte in last slot
    dec r9 ;Decrement memory index
    mov r11,1;r11 will store the size of our string stored in buffer we will use it while printing as argument to sys_write
.loop_block:
    mov rdx,0
    div rbx    ;Get the LSB by dividing number by 10 , LSB will be remainder (stored in 'dl') lik
    cmp rax,0 ;If rax (quotient) becomes 0 our procedure reached to the MSB of the number we sho
    je .return_block
    add dl,48 ;Convert each digit to its ASCII value
    mov [r9],dl ;Store the ASCII value in memory by using r9 as index
    dec r9 ;Dont forget to decrement r9 remember we are using memory backwards
    inc r11 ;Increment size as soon as you add a digit in memory
    jmp .loop_block ;Loop until it breaks on its own
.return_block:
    add dl,48 ;Don't forget to repeat the routine for out last MSB as loop ended early
    mov [r9],dl
    ;dec r9
    inc r11
    ret
    
strlen:;get length of strings based on null bytes
    xor rdi,rdi
.strlen_loop:
    cmp byte [rax + rdi],0x0
    je .strlen_break
    inc rdi
    jmp .strlen_loop
.strlen_break:
    inc rdi
    ret
    
exit:
    mov rax,60;select syscall sys_exit
    mov rdi,0;set error code to 0
    syscall
    
error_exit:
    mov rax,60
    mov rdi,1
    syscall