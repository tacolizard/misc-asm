section .data
	newline db 0xA,0x0
	
	com db 'Command? ',0x0

	fullstr db 'You have full health.',0x0
	medstr db 'You have medium health.',0x0
	lowstr db 'You have low health.',0x0
	deadstr db 'You have died!',0x0

	welcome db 'Welcome to my adventure!',0x0

	encounterstr db 'You see a monster.',0x0
	combatstr db 'The monster attacks.',0x0
	atkstr db 'You attack the monster.',0x0
	mdeadstr db 'The monster is slain.',0x0

section .bss
	align 4
	input resd 1 ;input buffer
	health resd 1 ;player health
	dmg resd 1 ;player dmg
	mhealth resd 1 ;monster health
	mdmg resd 1 ;monster dmg

section .text
	global _start

_start:
	call init
	mov rdi,welcome
	call println
	call encounter
	
	jmp exit

init:
	mov dword [health],100
	mov dword [dmg],5
	ret

tellHealth: ;inform the player of how much health they have
	cmp dword [health],0
	je .nohealth
	cmp dword [health],25
	jle .lowhealth
	cmp dword [health],50
	jge .medhealth
.fullhealth:
	mov rdi,fullstr
	call println
	ret
.nohealth:
	mov rdi,deadstr
	call println
	jmp exit
.lowhealth:
	mov rdi,lowstr
	call println
	ret
.medhealth:
	cmp dword [health],100
	je .fullhealth
	mov rdi,medstr
	call println
	ret

encounter: ;monster encounter
	mov dword [mhealth],10
	mov dword [mdmg],3
	mov rdi,encounterstr
	call println
.combat:
	cmp dword [mhealth],0
	jle .monsterdead
	mov rdi,combatstr
	call println
	call tellHealth
	call getCom
	cmp dword [input],'attack'
	je .attack
	jmp .combat
.attack:
	mov rdi,atkstr
	call println
	sub dword [mhealth],dmg
	jmp .combat
.monsterdead:
	mov rdi,mdeadstr
	call println
	ret
	

getCom: ;get command from user
	mov rdi,com
	call print
	call getInput
	ret

getInput: ;get input from stdin and store in input buffer
	mov rax,0
	mov rdi,0
	mov rsi,input
	mov rdx,16
	syscall
	ret

print:;print without newline
	push rdi
	call strlen
	mov rdi,1
	pop rsi
	mov rdx,rax
	mov rax,1
	syscall
	ret

println:;print with a newline
	call print
	mov rdi,newline
	call print
	ret

strlen: ;get length of strings that end with 0x0
	xor rax,rax ;zero rax
.strlen_loop:
	cmp BYTE [rdi + rax],0x0
	je .strlen_break
	inc rax
	jmp .strlen_loop
.strlen_break:
	inc rax
	ret

exit:
	mov rax,60
	mov rdi,0
	syscall

errExit:
	mov rax,60
	mov rdi,1
	syscall
