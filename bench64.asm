section .data
	running db 'Running',0xa
	period db '.',0xa
	done db 'Done.',0xa
	
	BYTE_BUFFER times 10 db 0 ;a buffer to be used by int_to_char

section .text
	global _start

_start:
	mov r12,500

	mov rdi,running
	call print

countdown:
	mov rax,r12
	call print_int

	sub r12,1
	cmp r12,0
	jne countdown
	jmp exit

print:
	push rdi
	call strlen
	mov rdi,1
	pop rsi
	mov rdx,rax
	mov rax,1
	syscall
	ret

print_int:
	call int_to_char
	mov rax,1
	mov rdi,1
	mov rsi,r9
	mov rdx,r11
	syscall
	ret

strlen:
	xor rax,rax
.strlen_loop:
	cmp BYTE [rdi + rax],0xa
	je .strlen_break
	inc rax
	jmp .strlen_loop
.strlen_break:
	inc rax
	ret

;converts integer to string
;takes int in rax
;returns pointer to string in r9
int_to_char:
	mov rbx,10
	mov r9,BYTE_BUFFER+10;store the number backwardes with LSB at 10 and decrementing to reach MSB
	mov [r9],byte 0 ;store null terminating byte in last slot
	dec r9;dec buffer index
	mov [r9],byte 0xa;store break line
	dec r9;dec index again
	mov r11,2;r11 will store the size of the string in the buffer.
.loop_block:
	mov rdx,0
	div rbx;get LSB by dividing by 10. LSB will be remainder and stored in dl. 
	cmp rax,0 ;if rax (quotient) is 0 then that means we have reached the MSB
	je .return_block
	add dl,48;convert each digit to ASCII
	mov [r9], dl;store ASCII value in memory with r9 as index
	dec r9;decrement our index
	inc r11; increment size of buffer to accomodate the new data
	jmp .loop_block ;loop
.return_block:
	add dl,48
	mov [r9], dl
	dec r9
	inc r11
	ret

exit:
	mov rax,60
	mov rdi,0
	syscall
