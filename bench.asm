section .text
global _start

_start:
	mov edx,startlen
	mov ecx,startmsg
	mov ebx,1
	mov eax,4
	int 0x80

	mov edi,999999

subloop:
	mov edx,perlen
	mov ecx,period
	mov ebx,1
	mov eax,4
	int 0x80

	sub edi,1
	cmp edi,0
	jne subloop

exit:
	mov edx,exitlen
	mov ecx,exitmsg
	mov ebx,1
	mov eax,4
	int 0x80

	mov ebx,0
	mov eax,1
	int 0x80

section .data

exitmsg db 0xa,'Done.',0xa
exitlen equ $ - exitmsg

startmsg db 'Running'
startlen equ $ - startmsg

period db '.'
perlen equ $ - period
