section 	.text
global 		_start

_start:

push msg
push len
jmp print
jmp exit

print: ;takes two args, message and length of message
	pop edx;get length argument
	pop ecx;get message argument
	mov ebx,1;file desc (stdout)
	mov eax,4;syscall number (sys_write)
	int 0x80;call kernel

exit:
	mov ebx,0 ;set exit code 0
	mov eax,1;call number (sys_exit)
	int 0x80;call kernel

section 	.data

msg db 'Hello, world!',0xa
len equ $ - msg ;len of str
