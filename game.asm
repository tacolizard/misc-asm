section .data
	pound db '#'
	poundlen equ $ - pound

	ship db 'o'
	shiplen equ $ - ship

	newline db 0xA
	nlen equ $ - newline

section .text
	global _start

_start:
	mov r9,0 ;xpos of ship

	mov r8,50;number of times to loop
poundline:
	call printPound
	sub r8,1
	cmp r8,0
	jne poundline

	call printNewline;just makes it look cleaner because the EOF isnt displayed
	jmp exit


printPound:
	mov rdi,pound
	mov rdx,poundlen
	call print
	ret

printNewline:
	mov rdi,newline
	mov rdx,nlen
	call print
	ret

printShip:
	mov rdi,ship
	mov rdx,shiplen
	call print
	ret

print:
	push rdi
	mov rdi,1
	pop rsi
	mov rax,1
	syscall
	ret

exit:
	mov rax,60
	mov rdi,0
	syscall
