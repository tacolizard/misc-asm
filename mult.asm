section .text
global _start

_start:
	mov eax,2	;numbers to multiply by
	mov ebx,4

	mov esp,eax;this is to check the answer
	imul esp,ebx

mult:
	add edi,eax
	dec ebx
	cmp ebx,0
	jne mult

	cmp edi,esp
	jne exit

	mov edx,len
	mov ecx,msg
	mov ebx,1
	mov eax,4
	int 0x80


exit:
	mov eax,1
	int 0x80

section .data

msg db 'True',0xa
len equ $ - msg
